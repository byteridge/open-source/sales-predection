import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.svm import LinearSVR
from sklearn.metrics import mean_squared_error
import xgboost as xgb
from fancyimpute import IterativeImputer
import pickle
import json


def predict(index):

    features=pd.read_csv('Features data set.csv')
    sales=pd.read_csv('sales data-set.csv')
    stores=pd.read_csv('stores data-set.csv')

    features['Date'] = pd.to_datetime(features['Date'])
    sales['Date'] = pd.to_datetime(sales['Date'])

    df=pd.merge(sales,features, on=['Store','Date', 'IsHoliday'], how='left')
    df=pd.merge(df,stores, on=['Store'], how='left')

    df=df.fillna(0)
    df['Temperature'] = (df['Temperature']- 32) * 5./9.

    types_encoded, types =df['Type'].factorize()
    df['Type'] = types_encoded
    df.drop_duplicates(inplace=True)


    df_average_sales_week = df.groupby(by=['Date'], as_index=False)['Weekly_Sales'].sum()
    df_average_sales = df_average_sales_week.sort_values('Weekly_Sales', ascending=False)
    #plt.figure(figsize=(20,5))
    #plt.plot(df_average_sales_week.Date, df_average_sales_week.Weekly_Sales)
    #plt.show()

    df_average_sales_week.Weekly_Sales = df_average_sales_week.Weekly_Sales/1000000
    data = df[['Weekly_Sales','Temperature','Fuel_Price','IsHoliday','CPI','Unemployment']]

    #sns.heatmap(data.corr(), annot = True)

    #############Using XGBoost Regressor#################
    sales_date_store = sales.groupby(["Date","Store"]).agg({"Weekly_Sales":"sum"})
    sales_date_store.sort_index(inplace=True)
    sales_date_store.Weekly_Sales = sales_date_store.Weekly_Sales/10000
    sales_date_store.Weekly_Sales = sales_date_store.Weekly_Sales.apply(int)
    data_table = pd.merge(features,sales_date_store ,  how='left', on=["Date","Store"])
    data_table = pd.merge(data_table,stores[["Store","Type"]] ,  how='left', on=["Store"])
    data_train = data_table[data_table.Weekly_Sales.notnull()]
    data_test = data_table[data_table.Weekly_Sales.isnull()]
    data_table['Date'] = pd.to_datetime(data_table.Date, format='%Y-%m-%d')

    itt = IterativeImputer()
    df = itt.fit_transform(data_table[["MarkDown1","MarkDown2","MarkDown3","MarkDown4","MarkDown5"]]) 
    data_table.MarkDown1 = df[:,0]
    data_table.MarkDown2 = df[:,1]
    data_table.MarkDown3 = df[:,2]
    data_table.MarkDown4 = df[:,3]
    data_table.MarkDown5 = df[:,4]

    data_table['CPI'].fillna((data_table['CPI'].mean()), inplace=True)
    data_table['Unemployment'].fillna((data_table['Unemployment'].mean()), inplace=True)
    data_table['IsHoliday'] = data_table['IsHoliday'].map({True:0,False:1})

    data_table["Month"] = data_table.Date.dt.month
    data_table["Year"] = data_table.Date.dt.year
    data_table["WeekofYear"] = data_table.Date.dt.weekofyear
    data_table.drop(['Date'],axis=1,inplace=True)
    
    #'data' : data_table.iloc[].to_json(orient = "records")
    sample_data = []
    if(index < 0): 
        a = np.random.randint(1,304,5)
        for i in a:
            temp = data_table.iloc[i:i+1,:].to_json(orient = "records")
            temp = json.loads(temp)[0]
            temp['index'] = int(i)
            sample_data.append(temp)
        #sample_data = data_table.iloc[a:a+5,:]
        return sample_data

    print("************************************",index)

    data_table = pd.get_dummies(data = data_table, columns = ['Type','Month','Year','WeekofYear'])
    data_train = data_table[data_table.Weekly_Sales.notnull()]
    validate_table = data_train.iloc[:305,:]

    sample_data = validate_table.iloc[index:index+1,:]

    data_train = data_train.iloc[306:,:] 
    data_test = data_table[data_table.Weekly_Sales.isnull()]

    X = data_train.drop('Weekly_Sales', axis=1)
    y = data_train['Weekly_Sales']

    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

    xg_reg = xgb.XGBRegressor()

    xg_reg.fit(X_train,y_train)

    score = xg_reg.score(X_test,y_test)

    y_pred = xg_reg.predict(X_test)
    rmse = np.sqrt(mean_squared_error(y_test, y_pred))
 

    #X = validate_table.drop('Weekly_Sales', axis=1)
    #y = validate_table['Weekly_Sales']
    #y_pred = xg_reg.predict(X)

    #For sample one record
    X_sample = sample_data.drop('Weekly_Sales', axis=1)
    y_sample = sample_data['Weekly_Sales']
    sample_pred = xg_reg.predict(X_sample)

    return {'pred' : sample_pred.tolist()[0], 'rmse' : rmse.tolist(), 'score' : score.tolist()}


    
