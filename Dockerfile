FROM ubuntu:18.04


RUN apt -y update &&\
    apt -y install python3 python3-pip

RUN python3 -m pip install --upgrade pip

RUN mkdir /app

WORKDIR /app

ADD . /

ADD . .
 
RUN python3 -m pip install -r python_requirements.txt

EXPOSE 5000

CMD ["python3","./web/server.py"]




