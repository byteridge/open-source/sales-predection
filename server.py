import numpy as np
import matplotlib.pyplot as plt
from flask import Flask, request, jsonify, render_template
import pickle
from prediction import predict

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/prediction/<index>',methods=['GET'])
def prediction(index):

   temp = predict(int(index))
   #print(temp)
   return jsonify(temp)
   
@app.route('/sampleData',methods=['GET'])
def sampleData():

   temp = predict(-1)
   #print(temp)
   return jsonify(temp)

if __name__ == '__main__':
    app.run(debug = "true", host = '0.0.0.0', port = 8080)